import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './App.css';
import createStudent from './components/createStudent';
import editeStudent from './components/editeStudent';
import studentList from './components/studentList';

import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


import { BrowserRouter, Switch, Link, Route } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar bg="dark" variant="dark">
          <Container>

            <Navbar.Brand >
              <Link to={"/createStudent"} className="nav-link">Create mean stuck App</Link>
            </Navbar.Brand>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/createStudent"} className="nav-link">Create student</Link>
              </Nav>

              {/*  <Nav>
                <Link to={"/editStudent/:id"} className="nav-link"> Edit Student </Link>
              </Nav>*/}

              <Nav>
                <Link to={"/studentList"} className="nav-link">Student List</Link>
              </Nav>

            </Nav>
          </Container>
        </Navbar>
      </header>

      <Container>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path="/" component={createStudent} />
                <Route path="/createStudent" component={createStudent} />
                <Route path="/editeStudent/:id" component={editeStudent} />
                <Route path="/studentList" component={studentList} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>

    </div>
  );
}

export default App;
