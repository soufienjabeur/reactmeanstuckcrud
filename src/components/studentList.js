import React, { Component } from 'react'
import axios from 'axios';
import StudentTableRow from './StudentTableRow';
import Table from 'react-bootstrap/Table'

export default class studentList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            students: []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/students/')
            .then(res => {
                this.setState({ students: res.data });
            })
            .catch((error) => {
                console.log(error);
            })
    }

    DataTable() {
        return this.state.students.map((res, i) => {
            return <StudentTableRow obj={res} key={i} />;
        });
    }
    render() {
        return (
            <div className="table-wrapper">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>RollNo</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.DataTable()}
                    </tbody>
                </Table>
            </div >
        );
    }

}
